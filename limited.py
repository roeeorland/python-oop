
class LimitedList:

    def __init__(self,   max_size=3,):
        self.max_size = max_size
        self.my_list = []



    def __repr__(self):
        return repr(self.my_list)

    def append(self, item):
        self.my_list.append(item)
        if len(self.my_list) > self.max_size:
            self.my_list = self.my_list[1:]

    def __setitem__(self, key, item):
        if key > self.max_size - 1:
            print('out of bounds')
        else:
            self.my_list[key] = item

    def __getitem__(self, key):
        return self.my_list[key]


def main():
    l1 = LimitedList(3)

    #assert l1 == []
    #print(l1)
    l1.append(1)
    l1.append(2)
    assert l1.__repr__() == str([1, 2])
    l1.append(3)
    assert l1.__repr__() == str([1, 2, 3])
    l1.append(4)
    assert l1.__repr__() == str([2, 3, 4])
    l1[1] = "three"
    assert l1.__repr__() == str([2, "three", 4])
    assert l1[1] == "three"




if __name__ == "__main__":
    main()


