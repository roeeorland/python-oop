
class WickedDictionary:

    def __init__(self):

        self.my_dict = {}

    def __repr__(self):
        return str(self.my_dict)

    def __setitem__(self, key, item):
        self.my_dict[key * 2] = item

    def __getitem__(self, key):
        return self.my_list[key]


def main():

    my_dict = WickedDictionary()
    my_dict["hello"] = 12
    assert my_dict.my_dict == {'hellohello': 12}
    my_dict[2] = 14
    assert my_dict.my_dict == {'hellohello': 12, 4: 14}


if __name__ == "__main__":
    main()


